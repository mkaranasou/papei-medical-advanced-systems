/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     10/8/2013 3:45:01 ��                         */
/*==============================================================*/



-- Type package declaration
create or replace package PDTypes  
as
    TYPE ref_cursor IS REF CURSOR;
end;

-- Integrity package declaration
create or replace package IntegrityPackage AS
 procedure InitNestLevel;
 function GetNestLevel return number;
 procedure NextNestLevel;
 procedure PreviousNestLevel;
 end IntegrityPackage;
/

-- Integrity package definition
create or replace package body IntegrityPackage AS
 NestLevel number;

-- Procedure to initialize the trigger nest level
 procedure InitNestLevel is
 begin
 NestLevel := 0;
 end;


-- Function to return the trigger nest level
 function GetNestLevel return number is
 begin
 if NestLevel is null then
     NestLevel := 0;
 end if;
 return(NestLevel);
 end;

-- Procedure to increase the trigger nest level
 procedure NextNestLevel is
 begin
 if NestLevel is null then
     NestLevel := 0;
 end if;
 NestLevel := NestLevel + 1;
 end;

-- Procedure to decrease the trigger nest level
 procedure PreviousNestLevel is
 begin
 NestLevel := NestLevel - 1;
 end;

 end IntegrityPackage;
/


drop trigger "CompoundDeleteTrigger_doctor"
/

drop trigger "CompoundInsertTrigger_doctor"
/

drop trigger "CompoundUpdateTrigger_doctor"
/

drop trigger "tib_doctor"
/

drop trigger "CompoundDeleteTrigger_patient"
/

drop trigger "CompoundInsertTrigger_patient"
/

drop trigger "CompoundUpdateTrigger_patient"
/

drop trigger "tib_patient"
/

drop trigger "CompoundDeleteTrigger_patient_"
/

drop trigger "CompoundInsertTrigger_patient_"
/

drop trigger "CompoundUpdateTrigger_patient_"
/

drop trigger "tib_patient_case"
/

drop trigger "CompoundDeleteTrigger_rad_cent"
/

drop trigger "CompoundInsertTrigger_rad_cent"
/

drop trigger "CompoundUpdateTrigger_rad_cent"
/

drop trigger "tib_rad_center"
/

drop trigger "CompoundDeleteTrigger_rad_orde"
/

drop trigger "CompoundInsertTrigger_rad_orde"
/

drop trigger "CompoundUpdateTrigger_rad_orde"
/

drop trigger "tib_rad_order"
/

drop trigger "CompoundDeleteTrigger_radiolog"
/

drop trigger "CompoundInsertTrigger_radiolog"
/

drop trigger "CompoundUpdateTrigger_radiolog"
/

drop trigger "tib_radiologist"
/

drop trigger "CompoundDeleteTrigger_xray"
/

drop trigger "CompoundInsertTrigger_xray"
/

drop trigger "CompoundUpdateTrigger_xray"
/

drop trigger "tib_xray"
/

alter table "Patient_Case"
   drop constraint FK_PATIENT__HAS_PATIENT
/

alter table "Rad_Order"
   drop constraint FK_RAD_ORDE_ISSUES_DOCTOR
/

alter table "Rad_Order"
   drop constraint FK_RAD_ORDE_RELATIONS_PATIENT_
/

alter table "Rad_Order"
   drop constraint FK_RAD_ORDE_RELATIONS_RAD_CENT
/

alter table "Rad_Order"
   drop constraint FK_RAD_ORDE_SCHEDULED_XRAY
/

alter table "Radiologist"
   drop constraint FK_RADIOLOG_RELATIONS_RAD_CENT
/

alter table "Xray"
   drop constraint FK_XRAY_EXECUTES_RADIOLOG
/

alter table "Xray"
   drop constraint FK_XRAY_SCHEDULED_RAD_ORDE
/

drop table "Doctor" cascade constraints
/

drop table "Patient" cascade constraints
/

drop index "Has_FK"
/

drop table "Patient_Case" cascade constraints
/

drop table "Rad_Center" cascade constraints
/

drop index "Scheduled_FK"
/

drop index "Relationship_4_FK"
/

drop index "Relationship_3_FK"
/

drop index "Issues_FK"
/

drop table "Rad_Order" cascade constraints
/

drop index "Relationship_5_FK"
/

drop table "Radiologist" cascade constraints
/

drop index "Executes_FK"
/

drop index "Scheduled2_FK"
/

drop table "Xray" cascade constraints
/

drop sequence "doctor_seq"
/

drop sequence "patient_case_seq"
/

drop sequence "patient_seq"
/

drop sequence "rad_center_seq"
/

drop sequence "rad_order_seq"
/

drop sequence "radiologist_seq"
/

drop sequence "xray_seq"
/

create sequence "doctor_seq"
increment by 1
start with 1
/

create sequence "patient_case_seq"
increment by 1
start with 1
/

create sequence "patient_seq"
increment by 1
start with 1
/

create sequence "rad_center_seq"
increment by 1
start with 1
/

create sequence "rad_order_seq"
increment by 1
start with 1
/

create sequence "radiologist_seq"
increment by 1
start with 1
/

create sequence "xray_seq"
increment by 1
start with 1
/

/*==============================================================*/
/* Table: "Doctor"                                              */
/*==============================================================*/
create table "Doctor" 
(
   "Doctor_Id"          INTEGER              not null,
   AIM                  CHAR(16)             not null,
   "First_Name"         CHAR(256)            not null,
   "Last_Name"          CHAR(256)            not null,
   "Email"              CHAR(256)            not null,
   "Organization_Unit"  INTEGER              not null,
   constraint PK_DOCTOR primary key ("Doctor_Id"),
   constraint AK_AIM_UK_DOCTOR unique (AIM)
)
/

/*==============================================================*/
/* Table: "Patient"                                             */
/*==============================================================*/
create table "Patient"
(
   "Patient_Id"         INTEGER              not null,
   "AMKA"               CHAR(16)             not null,
   "ADT"                CHAR(16)             not null,  
   "First_Name"         CHAR(256)            not null,
   "Last_Name"          CHAR(256)            not null,
   "Fathers_Name"       CHAR(256)            not null,
   "Birth_Date"         DATE                 not null,
   "Email"              CHAR(256),
   "Address_Line_1"     CHAR(256),
   "Address_Line_2"     CHAR(256),
   "ZIP_Code"           CHAR(256),
   "City"               CHAR(256),
   "Country"            CHAR(256),
   "Phone"              CHAR(10),
   constraint PK_PATIENT primary key ("Patient_Id"),
   constraint AK_AMKA_UK_PATIENT unique (AMKA),
   constraint AK_ADT_UK_PATIENT unique (ADT)
)
/

/*==============================================================*/
/* Table: "Patient_Case"                                        */
/*==============================================================*/
create table "Patient_Case" 
(
   "Patient_Case_Id"    INTEGER              not null,
   "Patient_Id"         INTEGER,
   "Patient_Class"      CHAR(1)              not null,
   "Doc_Notes"          CLOB,
   constraint PK_PATIENT_CASE primary key ("Patient_Case_Id")
)
/

/*==============================================================*/
/* Index: "Has_FK"                                              */
/*==============================================================*/
create index "Has_FK" on "Patient_Case" (
   "Patient_Id" ASC
)
/

/*==============================================================*/
/* Table: "Rad_Center"                                          */
/*==============================================================*/
create table "Rad_Center" 
(
   "Rad_Center_Id"      INTEGER              not null,
   "Name"               CHAR(256)            not null,
   constraint PK_RAD_CENTER primary key ("Rad_Center_Id")
)
/

/*==============================================================*/
/* Table: "Rad_Order"                                           */
/*==============================================================*/
create table "Rad_Order" 
(
   "Rad_Order_Id"       INTEGER              not null,
   "Doctor_Id"          INTEGER,
   "Rad_Center_Id"      INTEGER,
   "Xray_Id"            INTEGER,
   "Patient_Case_Id"    INTEGER,
   "Admission_Type"     CHAR(2)              not null,
   "CPT_Code"           CHAR(20)             not null,
   "CPT_Description"    CHAR(199)            not null,
   constraint PK_RAD_ORDER primary key ("Rad_Order_Id")
)
/

/*==============================================================*/
/* Index: "Issues_FK"                                           */
/*==============================================================*/
create index "Issues_FK" on "Rad_Order" (
   "Doctor_Id" ASC
)
/

/*==============================================================*/
/* Index: "Relationship_3_FK"                                   */
/*==============================================================*/
create index "Relationship_3_FK" on "Rad_Order" (
   "Patient_Case_Id" ASC
)
/

/*==============================================================*/
/* Index: "Relationship_4_FK"                                   */
/*==============================================================*/
create index "Relationship_4_FK" on "Rad_Order" (
   "Rad_Center_Id" ASC
)
/

/*==============================================================*/
/* Index: "Scheduled_FK"                                        */
/*==============================================================*/
create index "Scheduled_FK" on "Rad_Order" (
   "Xray_Id" ASC
)
/

/*==============================================================*/
/* Table: "Radiologist"                                         */
/*==============================================================*/
create table "Radiologist" 
(
   "Radiologist_Id"     INTEGER              not null,
   "Rad_Center_Id"      INTEGER,
   AAM                  CHAR(16)             not null,
   "First_Name"         CHAR(256)            not null,
   "Last_Name"          CHAR(256)            not null,
   constraint PK_RADIOLOGIST primary key ("Radiologist_Id"),
   constraint AK_AAM_UK_RADIOLOG unique (AAM)
)
/

/*==============================================================*/
/* Index: "Relationship_5_FK"                                   */
/*==============================================================*/
create index "Relationship_5_FK" on "Radiologist" (
   "Rad_Center_Id" ASC
)
/

/*==============================================================*/
/* Table: "Xray"                                                */
/*==============================================================*/
create table "Xray" 
(
   "Xray_Id"            INTEGER              not null,
   "Rad_Order_Id"       INTEGER              not null,
   "Radiologist_Id"     INTEGER              not null,
   "Image"              BLOB,
   "Rad_Report"         CLOB,
   "Doc_Report"         CLOB,
   "Appointment_Date"   DATE                 not null,
   constraint PK_XRAY primary key ("Xray_Id")
)
/

/*==============================================================*/
/* Index: "Scheduled2_FK"                                       */
/*==============================================================*/
create index "Scheduled2_FK" on "Xray" (
   "Rad_Order_Id" ASC
)
/

/*==============================================================*/
/* Index: "Executes_FK"                                         */
/*==============================================================*/
create index "Executes_FK" on "Xray" (
   "Radiologist_Id" ASC
)
/

alter table "Patient_Case"
   add constraint FK_PATIENT__HAS_PATIENT foreign key ("Patient_Id")
      references "Patient" ("Patient_Id")
/

alter table "Rad_Order"
   add constraint FK_RAD_ORDE_ISSUES_DOCTOR foreign key ("Doctor_Id")
      references "Doctor" ("Doctor_Id")
/

alter table "Rad_Order"
   add constraint FK_RAD_ORDE_RELATIONS_PATIENT_ foreign key ("Patient_Case_Id")
      references "Patient_Case" ("Patient_Case_Id")
/

alter table "Rad_Order"
   add constraint FK_RAD_ORDE_RELATIONS_RAD_CENT foreign key ("Rad_Center_Id")
      references "Rad_Center" ("Rad_Center_Id")
/

alter table "Rad_Order"
   add constraint FK_RAD_ORDE_SCHEDULED_XRAY foreign key ("Xray_Id")
      references "Xray" ("Xray_Id")
/

alter table "Radiologist"
   add constraint FK_RADIOLOG_RELATIONS_RAD_CENT foreign key ("Rad_Center_Id")
      references "Rad_Center" ("Rad_Center_Id")
/

alter table "Xray"
   add constraint FK_XRAY_EXECUTES_RADIOLOG foreign key ("Radiologist_Id")
      references "Radiologist" ("Radiologist_Id")
/

alter table "Xray"
   add constraint FK_XRAY_SCHEDULED_RAD_ORDE foreign key ("Rad_Order_Id")
      references "Rad_Order" ("Rad_Order_Id")
/


create or replace trigger "CompoundDeleteTrigger_doctor"
for  %EVENT% on "Doctor" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundInsertTrigger_doctor"
for  %EVENT% on "Doctor" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundUpdateTrigger_doctor"
for  %EVENT% on "Doctor" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create trigger "tib_doctor" before insert
on "Doctor" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Doctor_Id"" uses sequence doctor_seq
    select doctor_seq.NEXTVAL INTO :new."Doctor_Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create or replace trigger "CompoundDeleteTrigger_patient"
for  %EVENT% on "Patient" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundInsertTrigger_patient"
for  %EVENT% on "Patient" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundUpdateTrigger_patient"
for  %EVENT% on "Patient" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create trigger "tib_patient" before insert
on "Patient" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Patient_Id"" uses sequence patient_seq
    select patient_seq.NEXTVAL INTO :new."Patient_Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create or replace trigger "CompoundDeleteTrigger_patient_"
for  %EVENT% on "Patient_Case" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundInsertTrigger_patient_"
for  %EVENT% on "Patient_Case" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundUpdateTrigger_patient_"
for  %EVENT% on "Patient_Case" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create trigger "tib_patient_case" before insert
on "Patient_Case" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Patient_Case_Id"" uses sequence patient_case_seq
    select patient_case_seq.NEXTVAL INTO :new."Patient_Case_Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create or replace trigger "CompoundDeleteTrigger_rad_cent"
for  %EVENT% on "Rad_Center" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundInsertTrigger_rad_cent"
for  %EVENT% on "Rad_Center" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundUpdateTrigger_rad_cent"
for  %EVENT% on "Rad_Center" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create trigger "tib_rad_center" before insert
on "Rad_Center" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Rad_Center_Id"" uses sequence rad_center_seq
    select rad_center_seq.NEXTVAL INTO :new."Rad_Center_Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create or replace trigger "CompoundDeleteTrigger_rad_orde"
for  %EVENT% on "Rad_Order" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundInsertTrigger_rad_orde"
for  %EVENT% on "Rad_Order" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundUpdateTrigger_rad_orde"
for  %EVENT% on "Rad_Order" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create trigger "tib_rad_order" before insert
on "Rad_Order" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Rad_Order_Id"" uses sequence rad_order_seq
    select rad_order_seq.NEXTVAL INTO :new."Rad_Order_Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create or replace trigger "CompoundDeleteTrigger_radiolog"
for  %EVENT% on "Radiologist" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundInsertTrigger_radiolog"
for  %EVENT% on "Radiologist" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundUpdateTrigger_radiolog"
for  %EVENT% on "Radiologist" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create trigger "tib_radiologist" before insert
on "Radiologist" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Radiologist_Id"" uses sequence radiologist_seq
    select radiologist_seq.NEXTVAL INTO :new."Radiologist_Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/


create or replace trigger "CompoundDeleteTrigger_xray"
for  %EVENT% on "Xray" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundInsertTrigger_xray"
for  %EVENT% on "Xray" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create or replace trigger "CompoundUpdateTrigger_xray"
for  %EVENT% on "Xray" compound trigger
// Declaration
// Body
  before statement is
  begin
     NULL;
  end before statement;

  before each row is
  begin
     NULL;
  end before each row;

  after each row is
  begin
     NULL;
  end after each row;

  after statement is
  begin
     NULL;
  end after statement;

END
/


create trigger "tib_xray" before insert
on "Xray" for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column ""Xray_Id"" uses sequence xray_seq
    select xray_seq.NEXTVAL INTO :new."Xray_Id" from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/

