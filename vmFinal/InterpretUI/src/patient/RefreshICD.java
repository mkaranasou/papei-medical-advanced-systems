package patient;


import common.ADFUtil;

import java.util.Iterator;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adfinternal.view.faces.model.binding.FacesCtrlHierNodeBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.event.SelectionEvent;


public class RefreshICD {
    
    private RichTable tableICDCodes;
    private RichInputText selectedICDCode;
    private RichInputText selectedICDDescription;


    public RefreshICD() {     
        //tableICDCodes = new RichTable();
        selectedICDCode = new RichInputText(); 
        selectedICDDescription = new RichInputText(); 
    }

    private void refreshICDCodeAndDescription(){
        // Get the Selected Row key set iterator
        Iterator selectionIt = tableICDCodes.getSelectedRowKeys().iterator();
        //while(selectionIt.hasNext()){
            Object  rowKey = selectionIt.next();
            tableICDCodes.setRowKey(rowKey); int index = tableICDCodes.getRowIndex();
            FacesCtrlHierNodeBinding row = (FacesCtrlHierNodeBinding) tableICDCodes.getRowData(index);
            Row selectedRow = row.getRow();
        //}
                
        selectedICDCode.setValue(selectedRow.getAttribute("CptCode"));
        selectedICDDescription.setValue(selectedRow.getAttribute("CptDescr"));
    }
    
    public void codeSelectionListener(SelectionEvent selectionEvent) {
        ADFUtil.invokeEL("#{bindings.IcdCodesView1.collectionModel.makeCurrent}",
                         new Class[] { SelectionEvent.class },
                         new Object[] { selectionEvent });        
        refreshICDCodeAndDescription();
    }

    public void setTableICDCodes(RichTable tableICDCodes) {
        this.tableICDCodes = tableICDCodes;
    }

    public RichTable getTableICDCodes() {
        return tableICDCodes;
    }

    public void setSelectedICDCode(RichInputText selectedICDCode) {
        this.selectedICDCode = selectedICDCode;
    }

    public RichInputText getSelectedICDCode() {
        return selectedICDCode;
    }

    public void setSelectedICDDescription(RichInputText selectedICDDescription) {
        this.selectedICDDescription = selectedICDDescription;
    }

    public RichInputText getSelectedICDDescription() {
        return selectedICDDescription;
    }
}
