package userinfo;

import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

public class ScheduleAppointment {
    private RichInputText appointmentDateTime;
    private RichSelectOneChoice endTime;
    private RichSelectOneChoice startTime;
    private RichInputDate appointmentDate;

    public ScheduleAppointment() {
        appointmentDateTime = new RichInputText();
        appointmentDateTime.setValue("04-09-2013 09:20-09:40");
    }

    public void setAppointmentDateTime(RichInputText appointmentDateTime) {
        this.appointmentDateTime = appointmentDateTime;
    }

    public RichInputText getAppointmentDateTime() {
        //appointmentDateTime.setValue(appointmentDate.getValue()+" "+startTime.getValue()+"-"+endTime.getValue());
        appointmentDateTime.setValue("04-09-2013 09:20-09:40");
        return appointmentDateTime;
    }

    public void setEndTime(RichSelectOneChoice endTime) {
        this.endTime = endTime;
    }

    public RichSelectOneChoice getEndTime() {
        return endTime;
    }

    public void setStartTime(RichSelectOneChoice startTime) {
        this.startTime = startTime;
    }

    public RichSelectOneChoice getStartTime() {
        return startTime;
    }

    public void setAppointmentDate(RichInputDate appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public RichInputDate getAppointmentDate() {
        return appointmentDate;
    }

    public void changeAppointmentDate(ActionEvent actionEvent) {
        // Add event code here...
    }
}
