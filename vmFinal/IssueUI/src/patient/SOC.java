package patient;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public class SOC {
    
    private List<PatientClass> patientClassList;
        
    public SOC() {
        
        fillPatientClass();
    }
    
    private void fillPatientClass(){
        
        patientClassList = new ArrayList<PatientClass>();         
        patientClassList.add(new PatientClass("Emergency","E"));
        patientClassList.add(new PatientClass("Inpatient","I"));
        patientClassList.add(new PatientClass("Outpatient","O"));
        patientClassList.add(new PatientClass("Preadmit","P"));
        patientClassList.add(new PatientClass("Recurring Patient","R"));
        patientClassList.add(new PatientClass("Obstetrics","B"));
        
        
    }
    public List<SelectItem> getPatientClassSelectItems()
    { 
          List<SelectItem> patientClassSelectItems = new      ArrayList<SelectItem>();
          for (PatientClass patientClass: patientClassList) 
          { 
            //SelectItem constructor (value, label)
            SelectItem item = new SelectItem(patientClass, patientClass.getLabel());
            patientClassSelectItems.add(item); 
         } 
          return patientClassSelectItems; 
    } 

}
