package patient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.validator.ValidatorException;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.jdbc.OracleDriver;

public class PatientValidator {
    
    public PatientValidator() {        
    }

    public void validatePatient(FacesContext facesContext,
                                UIComponent uIComponent, Object object) {
        boolean loginOk = login_action(object.toString());
        
        if(!loginOk){
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                      "Λανθασμένα στοιχεία.",
                                                      null));
        }        

    }
    public boolean login_action(String patientCredential) {       
                
        Connection conn;

        try {
            conn = getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery ("SELECT ADT FROM Patient where AMKA = '"+patientCredential+"'");
            if (rset.next())  {                              
                conn.close();
                    return true;
            }
            conn.close();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return false;
    
    }

    public static Connection getConnection() throws SQLException {
        String username = "radiology";
        String password = "welcome1";
        String thinConn = "jdbc:oracle:thin:@localhost:1521:XE";
        DriverManager.registerDriver(new OracleDriver());
        Connection conn =
            DriverManager.getConnection(thinConn, username, password);
        conn.setAutoCommit(false);
        return conn;
    }


}
