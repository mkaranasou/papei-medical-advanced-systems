package patient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.jdbc.OracleDriver;

public class PatientInfo {
    
    private RichInputText patientCredential;   
    private RichInputText patientADT;
    private RichInputText amka;
    private RichInputText adt;
    private RichInputText lastname;
    private RichInputText firstname;
    private RichInputText fathersname;
    private RichInputText dateOfBirth;
    private RichInputText email;
    private RichInputText phone;
    private RichInputText mobile;
    private RichInputText address1;
    private RichInputText address2;
    private RichInputText city;
    private RichInputText state;
    private RichInputText postalCode;
    private RichInputText country;


    public PatientInfo() {
        super();
        
        patientCredential = new RichInputText();
        patientADT = new RichInputText();
          amka = new RichInputText();;
          adt = new RichInputText();;
          lastname = new RichInputText();;
          firstname = new RichInputText();;
          fathersname = new RichInputText();;
          dateOfBirth = new RichInputText();;
          email = new RichInputText();;
          phone = new RichInputText();;
          mobile = new RichInputText();;
          address1 = new RichInputText();;
          address2 = new RichInputText();;
          city = new RichInputText();;
          state = new RichInputText();;
          postalCode = new RichInputText();;
          country = new RichInputText();;
        
    }

    public boolean fillPatientInfo() {       
                
        String credential = patientCredential.getValue().toString();              
        Connection conn;

        try {
            conn = getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery ("SELECT * FROM Patient where AMKA = '"+credential+"' OR ADT = '"+ credential +"'");
            if (rset.next())  {       
                                                                                         
                patientADT.setValue(rset.getString("ADT"));
                amka.setValue(rset.getString("AMKA"));
                adt.setValue(rset.getString("ADT"));
                lastname.setValue(rset.getString("Last_Name"));
                firstname.setValue(rset.getString("First_Name"));
                fathersname.setValue(rset.getString("Fathers_Name"));
                dateOfBirth.setValue(rset.getString("Birth_Date").substring(0, 10));
                email.setValue(rset.getString("Email"));
                phone.setValue(rset.getString("Phone"));
                mobile.setValue(rset.getString("Mobile"));
                address1.setValue(rset.getString("Address_Line_1"));
                address2.setValue(rset.getString("Address_Line_2"));
                city.setValue(rset.getString("City"));
                state.setValue(rset.getString("State"));
                postalCode.setValue(rset.getString("ZIP_Code"));
                country.setValue(rset.getString("Country"));
              
                conn.close();
                    return true;
            }
            conn.close();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return false;
    
    }

    public static Connection getConnection() throws SQLException {
        String username = "radiology";
        String password = "welcome1";
        String thinConn = "jdbc:oracle:thin:@localhost:1521:XE";
        DriverManager.registerDriver(new OracleDriver());
        Connection conn =
            DriverManager.getConnection(thinConn, username, password);
        conn.setAutoCommit(false);
        return conn;
    }

    public void setPatientCredential(RichInputText patientCredential) {
        this.patientCredential = patientCredential;
    }

    public RichInputText getPatientCredential() {
        return patientCredential;
    }

    public void amkaChanged(ValueChangeEvent valueChangeEvent) {
        if(patientCredential.getValue() !=null)
        {
            fillPatientInfo();
        }
    }

    public void setPatientADT(RichInputText patientADT) {
        this.patientADT = patientADT;
    }

    public RichInputText getPatientADT() {
        return patientADT;
    }

    public void setAmka(RichInputText amka) {
        this.amka = amka;
    }

    public RichInputText getAmka() {
        return amka;
    }

    public void setAdt(RichInputText adt) {
        this.adt = adt;
    }

    public RichInputText getAdt() {
        return adt;
    }

    public void setLastname(RichInputText lastname) {
        this.lastname = lastname;
    }

    public RichInputText getLastname() {
        return lastname;
    }

    public void setFirstname(RichInputText firstname) {
        this.firstname = firstname;
    }

    public RichInputText getFirstname() {
        return firstname;
    }

    public void setFathersname(RichInputText fathersname) {
        this.fathersname = fathersname;
    }

    public RichInputText getFathersname() {
        return fathersname;
    }

    public void setDateOfBirth(RichInputText dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public RichInputText getDateOfBirth() {
        return dateOfBirth;
    }

    public void setEmail(RichInputText email) {
        this.email = email;
    }

    public RichInputText getEmail() {
        return email;
    }

    public void setPhone(RichInputText phone) {
        this.phone = phone;
    }

    public RichInputText getPhone() {
        return phone;
    }

    public void setMobile(RichInputText mobile) {
        this.mobile = mobile;
    }

    public RichInputText getMobile() {
        return mobile;
    }

    public void setAddress1(RichInputText address1) {
        this.address1 = address1;
    }

    public RichInputText getAddress1() {
        return address1;
    }

    public void setAddress2(RichInputText address2) {
        this.address2 = address2;
    }

    public RichInputText getAddress2() {
        return address2;
    }

    public void setCity(RichInputText city) {
        this.city = city;
    }

    public RichInputText getCity() {
        return city;
    }

    public void setState(RichInputText state) {
        this.state = state;
    }

    public RichInputText getState() {
        return state;
    }

    public void setPostalCode(RichInputText postalCode) {
        this.postalCode = postalCode;
    }

    public RichInputText getPostalCode() {
        return postalCode;
    }

    public void setCountry(RichInputText country) {
        this.country = country;
    }

    public RichInputText getCountry() {
        return country;
    }
}
