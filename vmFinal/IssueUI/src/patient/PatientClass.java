package patient;

public class PatientClass {
    
    String label;
    String value;
    
    public PatientClass() {
        super();
    }

    public PatientClass(String label, String value) {
        super();
        this.label = label;
        this.value = value;
    }


    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
