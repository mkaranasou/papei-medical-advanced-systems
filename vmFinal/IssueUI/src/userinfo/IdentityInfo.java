package userinfo;

import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.share.ADFContext;
import oracle.adf.share.security.SecurityContext;
import oracle.adf.share.security.identitymanagement.UserProfile;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.component.rich.output.RichOutputLabel;

import oracle.tip.pc.services.common.ServiceFactory;
import oracle.bpel.services.common.exception.ServicesException;
import oracle.bpel.services.common.util.WSClientConfig;

import oracle.bpel.services.identity.client.IdentityServiceSOAPClient;

import oracle.tip.pc.services.identity.BPMAuthenticationService;
import oracle.tip.pc.services.identity.BPMAuthorizationService;
import oracle.tip.pc.services.identity.BPMIdentityException;
import oracle.tip.pc.services.identity.BPMIdentityNotFoundException;
import oracle.tip.pc.services.identity.BPMIdentityService;

public class IdentityInfo {
    
    private RichOutputText username;  
    private RichOutputText userGroups;
    private RichOutputText userRoles;    
    
    
    private ADFContext adfCtx ;  
    private BPMAuthorizationService BPMservice ;
    private RichInputText doctorAMKA;
    private RichInputText lastName;
    private RichInputText firstName;

    public IdentityInfo() {
        adfCtx = ADFContext.getCurrent();
        BPMservice = ServiceFactory.getAuthorizationServiceInstance();
        
        username = new RichOutputText();
        userGroups = new RichOutputText();
        userRoles = new RichOutputText();
        firstName = new RichInputText();
        lastName = new RichInputText();
        doctorAMKA = new RichInputText();
            
        username.setValue(getUsername(adfCtx));
        userGroups.setValue(getUserGroupsAndRoles(BPMservice, username.getValue().toString(),0));
        userRoles.setValue(getUserGroupsAndRoles(BPMservice, username.getValue().toString(),1));
        
        //Firstname -> givenName , AMKA -> pager
        firstName.setValue(getFirstname(adfCtx));
        lastName.setValue(getLastname(adfCtx));
        doctorAMKA.setValue(getAMKA(adfCtx));
    }
    public String getUsername(ADFContext ctx){                                   
        return ctx.getSecurityContext().getUserPrincipal().getName();
    }
    public String getFirstname(ADFContext ctx){                                   
        UserProfile up = ctx.getSecurityContext().getUserProfile();
        if(up != null){
            return up.getBusinessStreet();
        }
        return null;
    }
    public String getLastname(ADFContext ctx){                                           
        UserProfile up = ctx.getSecurityContext().getUserProfile();
        if(up != null){
            return up.getDisplayName();
        }
        return null;
    }
    //AMKA -> pager
    public String getAMKA(ADFContext ctx){                                   
        return ctx.getSecurityContext().getUserProfile().getBusinessPager();
    }
    
    // grouprole = 0 -> group, 1-> role
    public String getUserGroupsAndRoles(BPMAuthorizationService service,String username,int grouprole ){
        
        String userGroups="";             
        
       /* Groups
            jazn.com/
          Roles
            OracleBPMProcessRolesApp/
        */
        try {           
               Object[] myArray = service.getGrantedRolesToUser(username, true).toArray();
     
            if(myArray.length >0 )
            {
                for(int i = 0 ; i<myArray.length;i++){
                                    
                    if(grouprole==0 && myArray[i].toString().startsWith(service.getRealmName())){
                        userGroups+=myArray[i].toString().substring(service.getRealmName().length()+1)+",";
                    }
                    else if(grouprole==1 && myArray[i].toString().startsWith("OracleBPMProcessRolesApp") && ! myArray[i].toString().startsWith("OracleBPMProcessRolesApp/RadiologyBPM") ){
                        userGroups+=myArray[i].toString().substring("OracleBPMProcessRolesApp/".length())+",";
                    }                    
                }    
                
                userGroups = userGroups.substring(0, userGroups.length()-1);
            }
        } catch ( BPMIdentityException e) {
        }
        
        return userGroups;
    }

    public void setUserRoles(RichOutputText userRoles) {
        this.userRoles = userRoles;
    }

    public RichOutputText getUserRoles() {
        return userRoles;
    }


    public void setUsername(RichOutputText username) {
        this.username = username;
    }

    public RichOutputText getUsername() {
        return username;
    }

    public void setUserGroups(RichOutputText userGroups) {
        this.userGroups = userGroups;
    }

    public RichOutputText getUserGroups() {
        return userGroups;
    }


    public void setDoctorAMKA(RichInputText doctorAMKA) {
        this.doctorAMKA = doctorAMKA;
    }

    public RichInputText getDoctorAMKA() {
        return doctorAMKA;
    }

    public void setLastName(RichInputText lastName) {
        this.lastName = lastName;
    }

    public RichInputText getLastName() {
        return lastName;
    }

    public void setFirstName(RichInputText firstName) {
        this.firstName = firstName;
    }

    public RichInputText getFirstName() {
        return firstName;
    }
}
